qtde_produtos = int(input("Quantos produtos foram vendidos? "));
comissao = double(0);

if (qtde_produtos >= 0 & qtde_produtos <= 250) then
    comissao = qtde_produtos * 1.00;
elseif (qtde_produtos > 250 & qtde_produtos <= 500) then
    comissao = qtde_produtos * 1.50;
elseif (qtde_produtos > 500) then
    comissao = qtde_produtos * 2.00;
else
    error("Quantidade de produtos digitada invalida. Digite uma quantidade maior ou igual a 0.\n");  
end

printf("O vendedor vendeu %d produtos e recebera uma comissao de R$ %f.", qtde_produtos, comissao);
