a = int(input("Digite um segmento de reta a: "));
b = int(input("Digite um segmento de reta b: "));
c = int(input("Digite um segmento de reta c: "));

la = %F;
lb = %F;
lc = %F;

if (abs(a-b) < c & c < (a+b)) then 
    lc = %T;  
end

if (abs(a-c) < b & b < (a+c)) then
    lb = %T;
end

if(abs(c-b) < a & a < (c+b)) then
    la = %T;
end

if (la & lb & lc ) then
    printf ("Existe um triangulo com lados a = %d, b = %d, c = %d.\n", a, b, c);
else 
    printf ("Não existe um triangulo com lados a = %d, b = %d, c = %d.\n", a, b, c);
end
