a = int(input("Digite um segmento de reta a: "));
b = int(input("Digite um segmento de reta b: "));
c = int(input("Digite um segmento de reta c: "));

if (a <= 0 | b <= 0 | c <= 0) then
    printf ("Nao existe triangulo com essas dimensoes.\n");
elseif (a == b & b == c) then
    printf ("O triangulo de lados a = %d, b = %d, c = %d eh Equilatero.\n", a,b,c);
elseif (a == b | b == c) then
    printf ("O triangulo de lados a = %d, b = %d, c = %d eh Isoceles.\n", a,b,c);
else
    printf ("O triangulo de lados a = %d, b = %d, c = %d eh Escaleno.\n", a,b,c);
end
