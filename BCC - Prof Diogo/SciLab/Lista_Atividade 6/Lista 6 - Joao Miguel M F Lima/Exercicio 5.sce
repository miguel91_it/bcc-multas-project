printf ("Dados da circunferencia: \n\n");

xc = double(input("Entre com a coordenada x do centro da circunferencia: "));
yc = double(input("Entre com a coordenada y do centro da circunferencia: "));
r = double(input("Entre com o raio da circunferencia: "));

printf ("Dados do ponto a verificar: \n\n");

xp = double(input("Entre com a coordenada x do ponto: "));
yp = double(input("Entre com a coordenada y do ponto: "));

dpc = sqrt( (xc - xp)^2 + (yc - yp)^2  );

//printf("dpc = %f | r = %f", dpc, r);

if (dpc <= r) then
    printf("O ponto P(%.1f, %.1f) esta dentro da circunferencia de centro C(%.1f, %.1f) e raio = %.1f", xp,yp,xc,yc,r);
end
