clc();
clear();

n_final = int(input("Digite um n final: "));

impar = 1;

positivo = %T;

pi = 0;

i = 0;

mcx = [1:1:n_final];
mcy = zeros(1,n_final);

pi_y = linspace(%pi, %pi, n_final);

while i < n_final
    
    if positivo then
        pi = pi + 4 / impar;
    else
        pi = pi - 4 / impar;
    end
    
    positivo = ~positivo;
    
    impar = impar + 2;
    
    i = i + 1;
    
    mcy(i) = pi;
end

printf ("pi = %f", pi);

plot(mcx,mcy,'b');
plot(mcx, pi_y, 'r');
