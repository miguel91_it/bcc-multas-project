clf
clear();
clc();

area_c = 0;
area_q = 0;

n = int(input("digite um numero de pontos: "));

mcx = [1:1:n];
mcy = zeros(1,n);

pi_y = linspace(%pi, %pi, n);

while area_q < n
    px = rand();
    py = rand();
    
    dc = sqrt ( (px - 0.5)^2 + (py - 0.5)^2  );
    
    if dc <= 0.5 then
        area_c = area_c + 1;
    end
    
    area_q = area_q + 1;
    
    pi = 4 * area_c / area_q;    
    
    mcy(area_q) = pi;
end



printf ("PI = %f", pi);

plot(mcx,mcy,'b');
plot(mcx, pi_y, 'r');
