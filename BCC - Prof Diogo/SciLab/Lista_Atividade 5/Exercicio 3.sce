emprestimo = double(input("Digite o valor do emprestimo tomado: R$ "));
taxa = double(input("Digite a taxa de juros percentual mensal: "));
tempo = int(input("Digite o tempo em meses do emprestimo: "));

val_final = double(emprestimo * (1 + taxa * tempo));

clc();

printf("Emprestimo de R$ %f a uma taxa de juros de %f mensal por %d meses = R$ %f total", emprestimo, taxa, tempo, val_final);
