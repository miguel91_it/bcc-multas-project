largura = double(input("Largura do paralelepipedo: "));
altura = double(input("Altura do paralelepipedo: "));
comprimento = double(input("Comprimento do paralelepipedo: "));

p = 4 * (altura + largura + comprimento);

a = 2 * (altura * largura + altura * comprimento + largura * comprimento);

v = altura * largura * comprimento;

diag1 = sqrt(largura^2 + comprimento^2);

d = sqrt(diag1^2 + altura^2);

printf("Perimetro: %f\n", p);
printf("Area: %f\n", a);
printf("Volume: %f\n", v);
printf("Diagonal: %f\n", d);
