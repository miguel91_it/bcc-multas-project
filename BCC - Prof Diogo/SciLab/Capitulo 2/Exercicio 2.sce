x_unidades = [20 60];
 
lucro = [0 200];

plot(x_unidades, (lucro ./ x_unidades), '*')

ylabel('Lucro por peça (R$)');
 
xlabel('Peças produzidas mensalmente (Unidades)');
 
title('Gráfico do lucro mensal por peça produzida');
