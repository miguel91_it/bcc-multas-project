//Grafico a)
x = -3:0.01:3;
f = exp(-x) - x;
for i=1:601 cte(1,i) = 0; end
plot2d(x, cte, style=[2])
plot2d(x, f, style=[5])
xlabel('x');
ylabel('y = e^(-x) - x');
set(gca(), 'grid', [1,1]);


//Grafico b)
x = -10:0.01:10;
y = sin(10*x) - cos(3*x);
for i=1:2001 cte(1,i) = 0; end
plot2d(x, y, style=[5]);
plot2d(x, cte, style=[1]);
xlabel('x');
ylabel('y = sen(10x) - cos(3*x)');

//Graficoc)
x = -10:0.01:10;
y = x - cos(x);
for i=1:2001 cte(1,i) = 0; end
plot(x,y,style=[4])
plot(x,cte,style=[2])
xlabel('x');
ylabel('y = x - cos9(x)');
