SNR = 0:0.01:100;
BER = (exp(-2*SNR))/2;
LOG_BER = log(BER);

BER_ATIVIDADE_2 = (exp(-SNR/2))/2;
LOG_BER_ATIVIDADE_2 = log(BER_ATIVIDADE_2);

LOG_BER;
LOG_BER_ATIVIDADE_2;

plot2d(SNR, [LOG_BER' LOG_BER_ATIVIDADE_2'], style=[2,5], leg="log(BER)@log(BER_ATIVIDADE_2)")

xlabel("SNR");
ylabel("log(BER)");
title("Gráfico Exercicio 1: BER em função da SNR")
set(gca(), 'grid', [1,1]);
